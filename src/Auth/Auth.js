import React, { useState } from "react";
import {
  signInWithEmailAndPassword,
  createUserWithEmailAndPassword,
  sendPasswordResetEmail,
} from "firebase/auth";
import { collection, addDoc } from "firebase/firestore";

import cn from "./Auth.module.scss";

const Auth = ({ auth, db }) => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [name, setName] = useState("");

  const [status, setStatus] = useState("");

  const logInWithEmailAndPassword = async (email, password) => {
    try {
      await signInWithEmailAndPassword(auth, email, password);
    } catch (err) {
      console.error(err);
      alert(err.message);
    }
  };

  const registerWithEmailAndPassword = async (name, email, password) => {
    try {
      const res = await createUserWithEmailAndPassword(auth, email, password);
      const user = res.user;
      await addDoc(collection(db, "users"), {
        uid: user.uid,
        name,
        authProvider: "local",
        email,
      });
    } catch (err) {
      console.error(err);
      alert(err.message);
    }
  };
  const register = () => {
    if (!name) alert("Please enter name");
    registerWithEmailAndPassword(name, email, password);
  };

  const sendPasswordReset = async (email) => {
    try {
      await sendPasswordResetEmail(auth, email);
      alert("Password reset link sent!");
    } catch (err) {
      console.error(err);
      alert(err.message);
    }
  };

  if (status === "reset") {
    return (
      <>
        <div className={cn.root}>
          <input
            type="text"
            className={cn.input}
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            placeholder="E-mail Address"
          />
          <button
            className={cn.button}
            onClick={() => sendPasswordReset(email)}
          >
            Send password reset email
          </button>
        </div>
        <button className={cn.buttonReg} onClick={() => setStatus("login")}>
          Log In
        </button>
      </>
    );
  }
  if (status === "reg") {
    return (
      <div className={cn.root}>
        <input
          type="text"
          className={cn.input}
          value={name}
          onChange={(e) => setName(e.target.value)}
          placeholder="Full Name"
        />
        <input
          type="text"
          className={cn.input}
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          placeholder="E-mail Address"
        />
        <input
          type="password"
          className={cn.input}
          value={password}
          onChange={(e) => setPassword(e.target.value)}
          placeholder="Password"
        />
        <button className={cn.button} onClick={register}>
          Register
        </button>
        <button className={cn.buttonReg} onClick={() => setStatus("reg")}>
          Already have an account?
        </button>
      </div>
    );
  }
  return (
    <>
      <div className={cn.root}>
        <input
          type="text"
          className={cn.input}
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          placeholder="E-mail Address"
        />
        <input
          type="password"
          className={cn.input}
          value={password}
          onChange={(e) => setPassword(e.target.value)}
          placeholder="Password"
        />
        <button
          className={cn.button}
          onClick={() => logInWithEmailAndPassword(email, password)}
        >
          Login
        </button>
        <button className={cn.buttonReg} onClick={() => setStatus("reg")}>
          Do not have an account?
        </button>
        <button className={cn.buttonReg} onClick={() => setStatus("reset")}>
          Forgot password
        </button>
      </div>
    </>
  );
};

export default Auth;
